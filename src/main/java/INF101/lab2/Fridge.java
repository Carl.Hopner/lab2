package INF101.lab2;

import java.util.List;
import java.util.NoSuchElementException;
import java.util.ArrayList;

public class Fridge implements IFridge {
    
    int max_size = 20;
    ArrayList<FridgeItem> FridgeContent = new ArrayList<>();



    @Override
    public int totalSize() {
        return max_size;
    }

    @Override
    public int nItemsInFridge() {
        return FridgeContent.size();
    }

    @Override
    public boolean placeIn(FridgeItem item) {
        if (nItemsInFridge()<20) {
            FridgeContent.add(item);
            return true;
        }
        else {
        return false;
        }
    }

    @Override
    public void takeOut(FridgeItem item) {
        if (FridgeContent.contains(item)) {FridgeContent.remove(item);}
        else {
            throw new NoSuchElementException();
        }
        
    }

    @Override
    public void emptyFridge() {
        FridgeContent.clear();
    }

    @Override
    public List<FridgeItem> removeExpiredFood() {
        ArrayList<FridgeItem> ExpiredItems = new ArrayList<>();
        for (FridgeItem item : FridgeContent) {
            if (item.hasExpired()) {
                ExpiredItems.add(item);
            }
        }
        for (FridgeItem item2 : ExpiredItems) {
            FridgeContent.remove(item2);
        }

        return ExpiredItems;
    }
}
